import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {
  Card,
  CardText,
  CardTitle,
  CardHeader
} from 'reactstrap'

import {Pill} from '../index'

export default class DayCard extends Component {

  static propTypes = {
    inverse: PropTypes.bool,
    title: PropTypes.string.isRequired,
    color: PropTypes.string,
    text: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ]),
    header: PropTypes.string,
    isEmpty: PropTypes.bool.isRequired,
    isToday: PropTypes.bool,
    isHeader: PropTypes.bool,
    dayClicked: PropTypes.func,
    date: PropTypes.instanceOf(Date)
  }

  static defaultProps = {
    header: 'Mon',
    color: 'primary',
    inverse: false,
    isToday: false,
    isHeader: false,
    text: '',
    dayClicked: () => null,
    date: new Date()
  }

  render() {
    const {
      title,
      text,
      header,
      color,
      isEmpty,
      inverse,
      isToday,
      isHeader,
      date,
      dayClicked
    } = this.props

    return (
      <Card body
            outline={!inverse}
            inverse={inverse}
            color={color}
            onClick={(e) => dayClicked(date, e)}
            className={`text-center ${(isEmpty || isHeader) ? 'd-none d-sm-block' : 'd-xl-block'}`} >
        <CardHeader className='d-sm-none'>
          {header}
          </CardHeader>
        <CardTitle>
          {

            isToday ? (
              <Pill text={title} />
            ) : (
              !isHeader ? (
                !isEmpty &&
                <Pill color="light" text={title}/>
              ) : (
                <h4 className='d-none d-sm-block'>{title}</h4>
              )
            )
          }
        </CardTitle>
        <CardText>
          {
            (!isHeader && !isEmpty) ? (
              <small className="text-muted">{text}</small>
            ) : (
              <small/>
            )
          }
          </CardText>
      </Card>
    )
  }
}
