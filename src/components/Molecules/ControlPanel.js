import React, { Component } from 'react'
import { Col, Row, Container, Button, ButtonGroup } from 'reactstrap'
import PropTypes from 'prop-types'

export default class ControlPanel extends Component {
  static propTypes = {
    date: PropTypes.instanceOf(Date).isRequired,
    month: PropTypes.bool.isRequired,
    dateName: PropTypes.string.isRequired,
    updateDate: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)

    this.changeDate = this.changeDate.bind(this)
  }

  changeDate(value) {
    // For month date skip
    if(this.props.month){
      this.props.updateDate(new Date(
        this.props.date.setMonth(this.props.date.getMonth() + value))
      )
    }
    // For day date skip
    else{
      this.props.updateDate(new Date(
        this.props.date.setDate(this.props.date.getDate() + value))
      )
    }
  }

  render() {
    const {
      date,
      dateName
    } = this.props

    return (
      <Container fluid>
        <Row>
          <Col xs={12} className='text-center'>
            <ButtonGroup>
            <Button onClick={(e) => this.changeDate(-1, e)} color='primary' outline>&laquo;</Button>
            <Button disabled color='primary'>{`${dateName} ${date.getFullYear()}`}</Button>
            <Button onClick={(e) => this.changeDate(1, e)} color='primary' outline>&raquo;</Button>
            </ButtonGroup>
          </Col>
        </Row>
      </Container>
    )
  }
}
