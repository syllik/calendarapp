import React, { Component } from 'react'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap'
import Locale, {
  changeLanguage
} from '../../utils/locale'
import PropTypes from 'prop-types'

export default class MenuPanel extends Component {
  static propTypes = {
    updateLang: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      isOpen: false
    }

    this.toggle = this.toggle.bind(this)
    this.changeLangClick = this.changeLangClick.bind(this)
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  changeLangClick(lang) {
    changeLanguage(lang)
    this.props.updateLang(this.state.language)
  }

  render() {
    return (
      <Navbar color="light" light expand="md" fixed={'top'}>
        <NavbarBrand href="/">{Locale.menuPanel_title}</NavbarBrand>
        <NavbarToggler onClick={this.toggle} />

        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                {Locale.menuPanel_view}
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem onClick={(e) => this.props.updateView(false, e)}>
                  {Locale.menuPanel_viewDay}
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem onClick={(e) => this.props.updateView(true, e)}>
                  {Locale.menuPanel_viewMonth}
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>

            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                {Locale.menuPanel_locale}
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem onClick={(e) => this.changeLangClick('ru', e)}>
                  {Locale.lang_ru}
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem onClick={(e) => this.changeLangClick('en', e)}>
                  {Locale.lang_en}
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>

            <NavItem >
              <NavLink href="https://bitbucket.org/syllik/calendarapp">{Locale.menuPanel_src}</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    )
  }
}
