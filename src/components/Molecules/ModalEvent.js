import React, { Component } from 'react'
import {
  Button,
  Modal,
  Label,
  Badge,
  FormGroup,
  ModalBody,
  ModalHeader,
  ModalFooter
} from 'reactstrap'
import {
  AvForm,
  AvGroup,
  AvInput,
  AvFeedback
} from 'availity-reactstrap-validation';
import _debounce from 'lodash.debounce'
import Locale from '../../utils/locale'

export default class ModalEvent extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
      inputTitle: this.props.event.text,
      inputDescription: this.props.event.desc,
      inputTime: this.props.event.time
    }

    this.toggleModal = this.toggleModal.bind(this)
    this.handleChangeTitle = this.handleChangeTitle.bind(this)
    this.handleChangeTime = this.handleChangeTime.bind(this)
    this.handleChangeDescription = this.handleChangeDescription.bind(this)
    this.saveEvent = this.saveEvent.bind(this)
    this.deleteEvent = this.deleteEvent.bind(this)
    this.formSubmit = this.formSubmit.bind(this)
  }

  formSubmit(event, errors, values) {
    this.setState({errors, values})
    if(errors.length === 0){
      this.saveEvent()
    }
  }

  validateTextArea = _debounce((value, ctx, input, cb) => {
    let maxWords = value.trim().split(/\s+/).length
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => {
      cb(value === '' || maxWords >= 5)
    }, 500)
  }, 300)

  toggleModal() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  handleChangeTitle(event) {
    this.setState({inputTitle: event.target.value})
  }

  handleChangeTime(event) {
    this.setState({inputTime: event.target.value})
  }

  handleChangeDescription(event){
    this.setState({inputDescription: event.target.value})
  }

  saveEvent (){
    let events = []
    let event = {
      text: this.state.inputTitle,
      time: this.state.inputTime,
      desc: this.state.inputDescription,
    }

    if(this.parseLocalStorage() !== null) {
      events = this.parseLocalStorage()
      events.map((e, i) => {
          if(e.time === this.props.event.time){
            events[i] = event
          }
        }
      )
    }
    if(this.props.isNewEvent){
      events.push(event)
    }

    this.props.updateEvents(events)
    this.setLocalStorage(events)
    this.toggleModal()
  }

  deleteEvent (){
    let events = this.parseLocalStorage()
    events.map((event, i) => {
        if(event.time === this.props.event.time){
          events.splice(i, 1)
        }
      }
    )
    if(!this.setLocalStorage(events)){
      this.removeLocalStorage()
    }
    this.props.updateEvents(events)
    this.toggleModal()
  }

  parseLocalStorage(){
    return JSON.parse(localStorage.getItem(this.props.getEventKey(this.props.date)))
  }

  setLocalStorage(value){
    localStorage.setItem(this.props.getEventKey(this.props.date), JSON.stringify(value))
    if(this.parseLocalStorage().length === 0){
      return false
    }
    return true
  }

  removeLocalStorage(){
    localStorage.removeItem(this.props.getEventKey(this.props.date))
  }

  render() {
    const {
      event,
      isNewEvent
    } = this.props

    return (
      <React.Fragment>
        <Button color="primary" size='sm' outline onClick={this.toggleModal}>
          {isNewEvent ? (Locale.text_addEvent) : (`${event.text} `)}
          <Badge color="primary">
            {event.time}
          </Badge>
        </Button>
        <Modal isOpen={this.state.isOpen} toggle={this.toggleModal}>
          <AvForm onSubmit={this.formSubmit}>
            <ModalHeader toggle={this.toggleModal}>
              {isNewEvent ? (Locale.text_headerAddEvent) : (Locale.text_headerEditEvent)}
            </ModalHeader>

            <ModalBody>
              <FormGroup>
                <AvGroup>
                  <Label for="title">{Locale.input_label_title}</Label>
                  <AvInput name="title" id="title" required
                           value={this.state.inputTitle}
                           placeholder={Locale.input_placeholder_title}
                           onChange={this.handleChangeTitle}
                  />
                  <AvFeedback>{Locale.field_required}</AvFeedback>
                </AvGroup>
              </FormGroup>
              <FormGroup>
                <AvGroup>
                  <Label for="time">{Locale.input_label_time}</Label>
                  <AvInput name="time" id="time" required
                           validate={{pattern: {value: /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/ }}}
                           value={this.state.inputTime}
                           onChange={this.handleChangeTime}
                  />
                  <AvFeedback>{Locale.field_time}</AvFeedback>
                </AvGroup>
              </FormGroup>
              <FormGroup>
                <AvGroup>
                  <Label for="decription">{Locale.input_label_description}</Label>
                  <AvInput type="textarea" name="decription" id="decription"
                           value={this.state.inputDescription}
                           onChange={this.handleChangeDescription}
                           validate={{async: this.validateTextArea}}
                  />
                  <AvFeedback>{Locale.input_additional_description}</AvFeedback>
                </AvGroup>
              </FormGroup>
            </ModalBody>

            <ModalFooter>
              <FormGroup>
                <Button color="primary">
                  {Locale.text_save}
                </Button>
              </FormGroup>
              <FormGroup>
                {!isNewEvent ? <Button color="danger" onClick={this.deleteEvent}>{Locale.text_delete}</Button> : <span/>}
              </FormGroup>
            </ModalFooter>

          </AvForm>
        </Modal>
      </React.Fragment>
    )
  }
}
