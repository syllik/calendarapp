import React, { Component } from 'react'
import {
  Badge
} from 'reactstrap'
import PropTypes from 'prop-types'

export default class Pill extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    color: PropTypes.string
  }

  static defaultProps = {
    color: 'primary'
  }

  render() {
    const {
      text,
      color
    } = this.props

    return (
      <Badge color={color} pill
             className='calendar-badge'>
        {text}
      </Badge>
    )
  }
}
