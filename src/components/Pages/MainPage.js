import React, { Component } from 'react'
import { MenuPanel, MonthTable, DayTable } from '../index'
import { Row } from 'reactstrap'
import Locale, { setLocale } from '../../utils/locale'

export class MainPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      lang: 'en',
      isMonthPage: true,
      dayClicked: new Date()
    }
  }

  updateLang = (value) => {
    this.setState({ lang: value })
  }

  updateView = (isMonthPage) => {
    this.setState({isMonthPage: isMonthPage})
  }

  dayClicked = (value) => {
    this.setState({
      isMonthPage: false,
      dayClicked: value
    })
  }

  componentWillMount(){
    setLocale()
  }

  getEventKey(date){
    return `${date.getDate()}${date.getMonth()}${date.getFullYear()}`
  }

  getHolidays(){
    return [
      { text: Locale.holiday_christmas, date: new Date(2019, 0, 7) },
      { text: Locale.holiday_defendersDay, date: new Date(2019, 1, 23) },
      { text: Locale.holiday_womansDay, date: new Date(2019, 2, 8) },
      { text: Locale.holiday_cosmosDay, date: new Date(2019, 3, 12) },
      { text: Locale.holiday_mayDay, date: new Date(2019, 4, 1) },
      { text: Locale.holiday_russiaDay, date: new Date(2019, 5, 12) },
      { text: Locale.holiday_kissDay, date: new Date(2019, 6, 6) },
      { text: Locale.holiday_vdvDay, date: new Date(2019, 7, 2) },
      { text: Locale.holiday_knowledgeDay, date: new Date(2019, 8, 1) },
      { text: Locale.holiday_halloween, date: new Date(2019, 9, 31) },
      { text: Locale.holiday_redDay, date: new Date(2019, 10, 7) },
      { text: Locale.holiday_newYear, date: new Date(2019, 11, 31) }
    ]
  }

  render() {
    return (
      <Row>
        <MenuPanel
          updateLang={this.updateLang}
          updateView={this.updateView}
        />
        {
          this.state.isMonthPage ? (
            <MonthTable
              dayClicked={this.dayClicked}
              getEventKey={this.getEventKey}
              getHolidays={this.getHolidays}
            />
          ) : (
            <DayTable
              date={this.state.dayClicked}
              getEventKey={this.getEventKey}
              getHolidays={this.getHolidays}
            />
          )
        }
      </Row>
    )
  }
}

export default MainPage
