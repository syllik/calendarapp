import React, { Component } from 'react'
import { Container, Col, CardGroup } from 'reactstrap'
import uuidv1 from  'uuid/v1'

import Locale, {
  getDayFullName,
  getMonthFullName
} from '../../utils/locale'

import {
  WEEK_DAYS,
  CURRENT_DATE,
  ROWS_NUM
} from '../../constants/Calendar'
import { DayCard, ControlPanel } from '../index'
import PropTypes from 'prop-types'

export default class MonthTable extends Component {
  static propTypes = {
    dayClicked: PropTypes.func.isRequired,
    getEventKey: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      date: new Date()
    }
  }

  getCalendarRow(date, isFirst){
    const calendarRow = []
    const cardCount = 0
    calendarRow.push(this.getCalendarDays(date, cardCount, isFirst))
    return calendarRow
  }

  getCalendarDays(date, cardCount, isFirst){
    const cards = []
    if(isFirst){

      // Sunday first day fix
      if(date.getDay() === 0 && cardCount === 0){
        for(let i = 0; i < 6; i++){
          cards.push(
            <DayCard title={date.getDate().toString()} isEmpty inverse={false} key={uuidv1()}/>
          )
          cardCount++
        }
      }
      // Push empty rows
      for (let i = 0; i < date.getDay() - 1; i++) {
        cards.push(
          <DayCard title={date.getDate().toString()} isEmpty inverse={false} key={uuidv1()}/>
        )
        cardCount++
      }
    }

    while (date.getMonth() === this.state.date.getMonth() && cardCount < WEEK_DAYS) {

      cards.push(
        <DayCard title={date.getDate().toString()}
                 header={getDayFullName(date.getDay())}
                 text={this.getEvents(this.props.getEventKey(date), date)}
                 color={this.checkWeekEnd(date.getDay()) ? 'warning' : 'primary'} isEmpty={false}
                 isToday={this.isTodayDay(date)} key={uuidv1()}
                 dayClicked={this.props.dayClicked}
                 date={new Date(date.getFullYear(), date.getMonth(), date.getDate())}
        />
      )
      cardCount++
      date.setDate(date.getDate() + 1)
    }

    if(!isFirst){
      while(cardCount < WEEK_DAYS){
        cards.push(
          <DayCard title={date.getDate().toString()} isEmpty key={uuidv1()}/>
        )
        cardCount++
      }
    }
    return cards
  }

  generateCalendar(){
    const date = new Date(this.state.date.getFullYear(), this.state.date.getMonth())
    const calendar = []
    let isFirstRow = true
    let max_rows = ROWS_NUM

    if(this.getDaysInMonth(date.getMonth(), date.getFullYear()) >= 31 && date.getDay() === 6){
      max_rows++
    }

    for (let rows = 0; rows < max_rows; rows++) {
      calendar.push(
        <CardGroup key={uuidv1()}>
          {this.getCalendarRow(date, isFirstRow)}
        </CardGroup>
      )
      if ((date.getDay() === 1 && date.getDate() === 2 && isFirstRow)) {
        max_rows++
      }
      if (rows !== 5)
        isFirstRow = false
    }
    return calendar
  }

  calendarTitle(text){
    return (
      <DayCard title={text} key={uuidv1()}isEmpty={false} inverse isHeader/>
    )
  }

  checkWeekEnd(day){
    if(day === 0 || day === 6){
      return true
    }
  }

  isTodayDay(date){
    if(date.getDate() === CURRENT_DATE.getDate() &&
      date.getMonth() === CURRENT_DATE.getMonth() &&
      date.getFullYear() === CURRENT_DATE.getFullYear()){
      return true
    }
  }

  updateDate = (value) => {
    this.setState({ date: value })
  }

  getEvents(key, date){
    const events = JSON.parse(localStorage.getItem(key))
    const viewEvents = []
    const HOLIDAYS = this.props.getHolidays()
    HOLIDAYS.forEach((holiday) => {
        if(holiday.date.getDate() === date.getDate() && holiday.date.getMonth() === date.getMonth())
          viewEvents.push(
            <span key={uuidv1()} className='text-success font-weight-bold'>{holiday.text}<br/></span>
          )
      }
    )
    if(events !== null){
      viewEvents.push(events.map((event) =>
        <span key={uuidv1()}>{event.text}<br/></span>
      ))
    }
    if(viewEvents.length !== 0){
      return viewEvents
    }
    return Locale.calendar_noEvent
  }

  getDaysInMonth(m, y) {
    m++
    return /8|3|5|10/.test(--m)?30:m===1?(!(y%4)&&y%100)||!(y%400)?29:28:31
  }

  render() {
    return (
      <Container fluid>
        <ControlPanel
          date={this.state.date}
          month={true}
          updateDate={this.updateDate}
          dateName={getMonthFullName(this.state.date.getMonth())}/>

        <Col xs={12}>
          <CardGroup>
            {this.calendarTitle(Locale.dayShort_1)}
            {this.calendarTitle(Locale.dayShort_2)}
            {this.calendarTitle(Locale.dayShort_3)}
            {this.calendarTitle(Locale.dayShort_4)}
            {this.calendarTitle(Locale.dayShort_5)}
            {this.calendarTitle(Locale.dayShort_6)}
            {this.calendarTitle(Locale.dayShort_0)}
          </CardGroup>
          {this.generateCalendar()}
        </Col>
      </Container>
    )
  }
}
