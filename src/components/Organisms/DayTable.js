import React, { Component } from 'react'
import {
  Container,
  Col,
  Table,
  Alert,
  ButtonGroup
} from 'reactstrap'
import uuidv1 from  'uuid/v1'

import Locale, {
  getDayFullName,
  getMonthFullName
} from '../../utils/locale'

import {
  HOURS,
  SEC_IN_HOUR
} from '../../constants/Calendar'

import { ControlPanel, ModalEvent } from '../index'
import PropTypes from 'prop-types'

export default class DayTable extends Component {
  static propTypes = {
    getEventKey: PropTypes.func.isRequired,
    date: PropTypes.instanceOf(Date),
    getHolidays: PropTypes.func.isRequired
  }

  static defaultProps = {
    date: new Date(),
  }

  constructor(props) {
    super(props)

    this.state = {
      events: JSON.parse(localStorage.getItem(this.props.getEventKey(this.props.date))),
      emptyEvent: {
        text: '',
        desc: '',
        time: ''
      }
    }
  }

  updateEvents = (value) => {
    this.setState({events: value})
  }

  updateDate = () => {
    this.setState({
      events: JSON.parse(localStorage.getItem(this.props.getEventKey(this.props.date)))
    })
  }

  generateTimeRow(time){
    let newTime = ''
    if(time < 10){
      newTime = `0${time}:00`
    }
    else{
      newTime = `${time}:00`
    }
    return newTime
  }

  generateTimeInSeconds(time){
    return new Date(`1970-01-01T${time}Z`).getTime() / 1000
  }

  isRowTimeEvent(time1, time2){
    // Checking if event in timerow
    let time1InSec = this.generateTimeInSeconds(time1)
    let time2InSec = this.generateTimeInSeconds(time2)
    return time1InSec <= time2InSec && time1InSec + SEC_IN_HOUR > time2InSec
  }

  generateDayTable(){
    const rows = []
    for (let time = 0; time<HOURS; time++){
      rows.push(
        <tr key={uuidv1()}>
          <th scope="row">{this.generateTimeRow(time)}</th>
          <td>
            {this.getEvents(this.generateTimeRow(time))}
            </td>
        </tr>
      )
    }

    return(
      <Col sm={12}>
        <Table striped hover responsive bordered size="sm" >
          <thead>
          <tr>
            <th>{Locale.dayPage_time}</th>
            <th>{Locale.dayPage_event}</th>
          </tr>
          </thead>
          <tbody>
          {rows}
          </tbody>
        </Table>
      </Col>
    )
  }

  getAlert(key, date){
    const alerts = []
    const HOLIDAYS = this.props.getHolidays()

    HOLIDAYS.forEach((holiday) => {
        if(holiday.date.getDate() === date.getDate() && holiday.date.getMonth() === date.getMonth())
          alerts.push(
            <Alert color="success" className='text-center' key={uuidv1()}>
              <b>{`${Locale.dayPage_celebrated} ${holiday.text}`}</b>
            </Alert>
          )
      }
    )
    if(this.state.events === null){
      alerts.push(
        <Alert color="warning" className='text-center' key={uuidv1()}>
          <b>{Locale.calendar_noEvent}</b>
        </Alert>
      )
    }
    return alerts
  }

  getEvents(rowTime){
    if(this.state.events !== null) {
      const viewEvents = this.state.events.map((event) =>
        this.isRowTimeEvent(rowTime, event.time) &&
        <React.Fragment key={uuidv1()}>
          <ModalEvent
            getEventKey={this.props.getEventKey}
            date={this.props.date}
            event={event}
            updateEvents={this.updateEvents}
            isNewEvent={false}
          />
        </React.Fragment>
      )

      return (
        <ButtonGroup vertical>
          {viewEvents}
        </ButtonGroup>
      )
    }
  }

  render() {
    const {
      date
    } = this.props

    return (
      <Container fluid>
        <ControlPanel
          date={date}
          month={false}
          updateDate={this.updateDate}
          dateName={`${getMonthFullName(date.getMonth())}, ${date.getDate()}`}/>

        <Col xs={12}>
          <div className='text-center'>
            <h1>
              {getDayFullName(this.props.date.getDay())}&nbsp;
            </h1>
            {this.getAlert(this.props.getEventKey(this.props.date), this.props.date)}
            <ModalEvent
              getEventKey={this.props.getEventKey}
              date={this.props.date}
              event={this.state.emptyEvent}
              updateEvents={this.updateEvents}
              isNewEvent
            />
          </div>
          {this.generateDayTable()}
        </Col>
      </Container>
    )
  }
}
