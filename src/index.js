import React from 'react'
import { render } from 'react-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import Routes from './routes'
import registerServiceWorker from './utils/registerServiceWorker'
import './index.css'
render(
    <Routes />,
  document.getElementById('root')
)
registerServiceWorker()
