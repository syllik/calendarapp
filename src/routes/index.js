import React from 'react'
import {
  MainPage
} from '../components'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Container } from 'reactstrap'

function Routes() {
  return (
    <Router>
      <Container fluid>
        <Route path="/" component={MainPage} />
      </Container>
    </Router>
  )
}

export default Routes
