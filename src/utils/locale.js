import LocalizedStrings from 'react-localization'

const Locale = new LocalizedStrings({
  en:{
    menuPanel_title:"Calendar",
    menuPanel_locale:"Change language",
    menuPanel_today:"Today",
    menuPanel_src:"Source code",
    menuPanel_view:"Change view",
    menuPanel_viewDay:"Day",
    menuPanel_viewMonth:"Month",
    lang_ru:"Russian",
    lang_en:"English",
    dayShort_0:"sun",
    dayShort_1:"mon",
    dayShort_2:"tue",
    dayShort_3:"wed",
    dayShort_4:"thu",
    dayShort_5:"fri",
    dayShort_6:"sat",
    dayFull_0:"Sunday",
    dayFull_1:"Monday",
    dayFull_2:"Tuesday",
    dayFull_3:"Wednesday",
    dayFull_4:"Thursday",
    dayFull_5:"Friday",
    dayFull_6:"Saturday",
    monthFull_0:"January",
    monthFull_1:"February",
    monthFull_2:"March",
    monthFull_3:"April",
    monthFull_4:"May",
    monthFull_5:"June",
    monthFull_6:"July",
    monthFull_7:"August",
    monthFull_8:"September",
    monthFull_9:"October",
    monthFull_10:"November",
    monthFull_11:"December",
    calendar_noEvent:"No events",
    holiday_christmas:"Christmas day",
    holiday_defendersDay:"Defender of the Fatherland Day",
    holiday_womansDay:"International Women's Day",
    holiday_cosmosDay:"Сosmonautics day",
    holiday_mayDay:"May day",
    holiday_russiaDay:"Russia day",
    holiday_kissDay:"Kissing day",
    holiday_vdvDay:"Airborne Forces Day",
    holiday_knowledgeDay:"Knowledge Day",
    holiday_halloween:"Halloween",
    holiday_redDay:"October Revolution Day",
    holiday_newYear:"New Year",
    dayPage_time:"Time",
    dayPage_event:"Event",
    dayPage_celebrated:"Celebrated today:",
    text_delete:"Delete",
    text_save:"Save",
    text_addEvent:"Add event",
    text_headerAddEvent:"Adding event",
    text_headerEditEvent:"Editing event",
    input_label_title:"Title",
    input_label_time:"Pick time",
    input_label_description:"Description",
    input_placeholder_title:"Enter title...",
    input_additional_description:"Enter 5 or more words, or leave this field empty",
    field_required:"Field should not be empty",
    field_time:"Enter valid time in format HH:mm!"
  },
  ru: {
    menuPanel_title:"Календарь",
    menuPanel_locale:"Сменить язык",
    menuPanel_today:"Сегодня",
    menuPanel_src:"Исходный код",
    menuPanel_view:"Сменить вид",
    menuPanel_viewDay:"День",
    menuPanel_viewMonth:"Месяц",
    lang_ru:"Русский",
    lang_en:"Английский",
    dayShort_0:"вс",
    dayShort_1:"пн",
    dayShort_2:"вт",
    dayShort_3:"ср",
    dayShort_4:"чт",
    dayShort_5:"пт",
    dayShort_6:"сб",
    dayFull_0:"Воскресенье",
    dayFull_1:"Понедельник",
    dayFull_2:"Вторник",
    dayFull_3:"Среда",
    dayFull_4:"Четверг",
    dayFull_5:"Пятница",
    dayFull_6:"Суббота",
    monthFull_0:"Январь",
    monthFull_1:"Февраль",
    monthFull_2:"Март",
    monthFull_3:"Апрель",
    monthFull_4:"Май",
    monthFull_5:"Июнь",
    monthFull_6:"Июль",
    monthFull_7:"Август",
    monthFull_8:"Сентябрь",
    monthFull_9:"Октябрь",
    monthFull_10:"Ноябрь",
    monthFull_11:"Декабрь",
    calendar_noEvent:"Нет событий",
    holiday_christmas:"Рождество",
    holiday_defendersDay:"День защитника отечества",
    holiday_womansDay:"Международный женский день",
    holiday_cosmosDay:"День космонавтики",
    holiday_mayDay:"Праздник весны и труда",
    holiday_russiaDay:"День России",
    holiday_kissDay:"День поцелуев",
    holiday_vdvDay:"День Воздушно-десантных войск",
    holiday_knowledgeDay:"День знаний",
    holiday_halloween:"День всех святых",
    holiday_redDay:"Красный день календаря",
    holiday_newYear:"Новый год",
    dayPage_time:"Время",
    dayPage_event:"Событие",
    dayPage_celebrated:"Сегодня празднуется:",
    text_delete:"Удалить",
    text_save:"Сохранить",
    text_addEvent:"Добавить событие",
    text_headerAddEvent:"Добавление события",
    text_headerEditEvent:"Редактирование события",
    input_label_title:"Заголовок",
    input_label_time:"Выберите время",
    input_label_description:"Описание",
    input_placeholder_title:"Введите заголовок...",
    input_additional_description:"Введите 5 или более слов, или оставьте поле пустым",
    field_required:"Поле не должно быть пустым!",
    field_time:"Введите верное время в формате ЧЧ:мм!",
  }
})

export function setLocale() {
  if(localStorage.getItem('locale') === null){
    localStorage.setItem('locale', Locale.getInterfaceLanguage())
    Locale.setLanguage(Locale.getInterfaceLanguage())
  }
  Locale.setLanguage(localStorage.getItem('locale'))
}

export function changeLanguage(lang) {
  Locale.setLanguage(lang)
  localStorage.setItem('locale', lang)
}

export function getDayShortName(day) {
  const SHORT_DAYS_NAME = [
    Locale.dayShort_0,
    Locale.dayShort_1,
    Locale.dayShort_2,
    Locale.dayShort_3,
    Locale.dayShort_4,
    Locale.dayShort_5,
    Locale.dayShort_6,
  ]
  return SHORT_DAYS_NAME[day]
}

export function getDayFullName(day) {
  const FULL_DAYS_NAME = [
    Locale.dayFull_0,
    Locale.dayFull_1,
    Locale.dayFull_2,
    Locale.dayFull_3,
    Locale.dayFull_4,
    Locale.dayFull_5,
    Locale.dayFull_6,
  ]
  return FULL_DAYS_NAME[day]
}

export function getMonthFullName(month) {
  const FULL_MONTH_NAME = [
    Locale.monthFull_0,
    Locale.monthFull_1,
    Locale.monthFull_2,
    Locale.monthFull_3,
    Locale.monthFull_4,
    Locale.monthFull_5,
    Locale.monthFull_6,
    Locale.monthFull_7,
    Locale.monthFull_8,
    Locale.monthFull_9,
    Locale.monthFull_10,
    Locale.monthFull_11,
  ]
  return FULL_MONTH_NAME[month]
}
export default Locale
